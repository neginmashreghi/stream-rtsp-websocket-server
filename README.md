# Description 
Nodejs application that will read the stream from the RTSP 

## Installation

Run the websocket at port 3008
```bash
node app.js
```


# Code Challenge 

Simple REACT page displaying Live stream from an IP camera. 

# Stream RTSP

There are 7 ways to stream RTSP on the pag. [read more](https://flashphoner.com/7-ways-to-stream-rtsp-on-the-page/ )  

One way is to  deliver a video stream Websocket transport (a TCP connection between the browser and the server). 

![Websocket](https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiBq8by6KbnAhU6g3IEHQbYB74QjRx6BAgBEAQ&url=https%3A%2F%2Fmedium.com%2Fplatform-engineer%2Fweb-api-design-35df8167460&psig=AOvVaw2msJMbZ6j6r5xpIkFykQ5j&ust=1580318728714761)

# Solution 

* on Server side Nodejs application that will read the stream from the RTSP 

* on client side a canvas that will get that stream from the nodejs application 


### Server side 
[node-rtsp-stream](https://www.npmjs.com/package/node-rtsp-stream ) is npm package that allows to Stream any RTSP stream (convert a RTSP stream into a MPEG-TS stream) and output to websocket for consumption by jsmpeg.(Requires ffmpeg) 

### Client side
[Jsmpeg](https://github.com/phoboslab/jsmpeg ) is a Javascript library that allows to visualize such stream into a <canvas> element
 
* HTML page Display RTSP stream
```bash
git clone https://neginmashreghi@bitbucket.org/neginmashreghi/display_rtsp_stream_html.git
```
* React page  Display RTSP stream
```bash
git clone https://neginmashreghi@bitbucket.org/neginmashreghi/display_rtsp_stream_react.git
```